"""TDEV URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from accounts import views as v

urlpatterns = [
    path(r'accounts/', include('accounts.urls')),
    path(r'home/', v.home),
    path(r'profile/', v.profile),
    path(r'user_manager/', v.account_manager),
    path(r'user_manager/create_user/', v.create_account),
    path(r'topic/', v.topic),
    path(r'appraisal_format/', v.appraisal_format),
    path(r'roles_functions/', v.roles_functions),
]
