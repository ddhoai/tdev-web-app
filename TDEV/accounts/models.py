from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Topic(models.Model):
    topic_name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    created_on = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        if self.id is None:
            self.id = self.__class__.objects.count()
        super(Topic, self).save(*args, **kwargs)


class Criteria(models.Model):
    criteria_name = models.CharField(max_length=200)

    def save(self, *args, **kwargs):
        if self.id is None:
            self.id = self.__class__.objects.count()
        super(Criteria, self).save(*args, **kwargs)


class AppraisalFormat(models.Model):
    format_name = models.CharField(max_length=200, default='default')
    criteria_name = models.CharField(max_length=200)

    def save(self, *args, **kwargs):
        if self.id is None:
            self.id = self.__class__.objects.count()
        super(AppraisalFormat, self).save(*args, **kwargs)


class Roles(models.Model):
    role_name = models.CharField(max_length=200)

    def save(self, *args, **kwargs):
        if self.id is None:
            self.id = self.__class__.objects.count()
        super(Roles, self).save(*args, **kwargs)


class Functions(models.Model):
    function_name = models.CharField(max_length=200)

    def save(self, *args, **kwargs):
        if self.id is None:
            self.id = self.__class__.objects.count()
        super(Functions, self).save(*args, **kwargs)


class RolesFunctions(models.Model):
    role = models.ForeignKey(Roles, on_delete=models.CASCADE)
    function = models.ForeignKey(Functions, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        if self.id is None:
            self.id = self.__class__.objects.count()
        super(RolesFunctions, self).save(*args, **kwargs)


class Session(models.Model):
    presenter = models.ForeignKey(User, on_delete=models.CASCADE)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    appraisal_format_name = models.CharField(max_length=200, default='default')
    date = models.DateTimeField()
    deadline = models.DateTimeField()

    def save(self, *args, **kwargs):
        if self.id is None:
            self.id = self.__class__.objects.count()
        super(Session, self).save(*args, **kwargs)


class Appraisal(models.Model):
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    attendee = models.ForeignKey(User, on_delete=models.CASCADE)
    criteria = models.ForeignKey(Criteria, on_delete=models.CASCADE)
    score = models.IntegerField(default=0)
    comment = models.CharField(max_length=200)

    def save(self, *args, **kwargs):
        if self.id is None:
            self.id = self.__class__.objects.count()
        super(Appraisal, self).save(*args, **kwargs)


role = models.ForeignKey(Roles, default=1, on_delete=models.CASCADE)
role.contribute_to_class(User, 'role')
