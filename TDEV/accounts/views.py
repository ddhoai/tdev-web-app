from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime
import MySQLdb
from django.contrib.auth.models import User
import copy
# Create your views here.


def check_permission(username, permission_id):
    db = MySQLdb.connect(user='root', db='mydb', passwd='123', host='localhost')
    logged_user = db.cursor(MySQLdb.cursors.DictCursor)
    query_string = str('SELECT role_id FROM auth_user WHERE username = "{}"').\
        format(username)
    logged_user.execute(query_string)

    for element in logged_user.fetchall():
        role_id = element['role_id']

    permission = db.cursor(MySQLdb.cursors.DictCursor)
    query_string = str('SELECT function_id FROM accounts_rolesfunctions WHERE role_id = "{}"'). \
        format(role_id)
    permission.execute(query_string)
    for element in permission.fetchall():
        if element['function_id'] == permission_id:
            return True
    return False


@login_required
def home(request):
    username = None
    if request.user.is_authenticated:
        username = request.user.get_username()
    return render(request, 'registration/index.html', {'username': username})


@login_required
def profile(request):
    username = None
    if request.user.is_authenticated:
        username = request.user.get_username()
    return render(request, 'registration/user-profile.html', {'username': username})


@login_required
def account_manager(request):
    username = None
    cursor = None

    if request.user.is_authenticated:
        username = request.user.get_username()

    if request.method == 'GET':
        db = MySQLdb.connect(user='root', db='mydb', passwd='123', host='localhost')
        cursor = db.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM auth_user ORDER BY id')

    else:
        pass

    return render(request, 'registration/account-manager.html', {'username': username, 'list_user': cursor.fetchall(),
                                                                 'allow': check_permission(username, 1)})


@csrf_exempt
@login_required
def create_account(request):
    username = None
    cursor = None
    if request.user.is_authenticated:
        username = request.user.get_username()
    if request.method == 'GET':
        pass
    if request.method == 'POST':
        db = MySQLdb.connect(user='root', db='mydb', passwd='123', host='localhost')
        cursor = db.cursor(MySQLdb.cursors.DictCursor)
        query_string = str('SELECT * FROM auth_user WHERE username = "{}" or email = "{}"').\
            format(request.POST['feUsername'], request.POST['feEmailAddress'])
        cursor.execute(query_string)
        if request.POST['fePassword'] == request.POST['feConfirmPassword'] and not cursor.fetchall():
            user = User.objects.create_user(username=request.POST['feUsername'],
                                            email=request.POST['feEmailAddress'],
                                            password=request.POST['fePassword'],
                                            first_name=request.POST['feFirstName'],
                                            last_name=request.POST['feLastName'])
            return HttpResponseRedirect("/user_manager/")
        else:
            return HttpResponseRedirect("/user_manager/create_user/")

    return render(request, 'registration/create_user.html', {'username': username})


@csrf_exempt
@login_required
def topic(request):
    username = None
    cursor = None
    allow = False
    if request.user.is_authenticated:
        username = request.user.get_username()
    if request.method == 'GET':
        db = MySQLdb.connect(user='root', db='mydb', passwd='123', host='localhost')
        cursor = db.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM accounts_topic ORDER BY id')

    if request.method == 'POST':
        db = MySQLdb.connect(user='root', db='mydb', passwd='123', host='localhost')
        cursor = db.cursor(MySQLdb.cursors.DictCursor)
        add_topic = ('INSERT INTO accounts_topic (topic_name, description, created_on)'
                     'VALUES(%s, %s, %s)')
        today = datetime.today()
        data_topic = (request.POST['feTopicName'], request.POST['feDescription'], today)
        cursor.execute(add_topic, data_topic)
        db.commit()
        cursor.close()

        return HttpResponseRedirect("/topic/")

    return render(request, 'registration/topic.html', {'username': username, "topic_list": cursor.fetchall(),
                                                       'allow': check_permission(username, 4)})


@csrf_exempt
@login_required
def appraisal_format(request):
    username = None
    cursor = None
    format_list = None
    format_set = set()
    if request.user.is_authenticated:
        username = request.user.get_username()
    if request.method == 'GET':
        db = MySQLdb.connect(user='root', db='mydb', passwd='123', host='localhost')
        cursor = db.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM accounts_criteria ORDER BY id')

        format_list = db.cursor(MySQLdb.cursors.DictCursor)
        format_list.execute('SELECT * FROM accounts_appraisalformat ORDER BY id')

        format_list_2 = db.cursor(MySQLdb.cursors.DictCursor)
        format_list_2.execute('SELECT * FROM accounts_appraisalformat ORDER BY id')
        for element in format_list_2.fetchall():
            format_set.add(element['format_name'])

    if request.method == 'POST':
        db = MySQLdb.connect(user='root', db='mydb', passwd='123', host='localhost')
        cursor = db.cursor(MySQLdb.cursors.DictCursor)
        for element in request.POST.getlist('list_criteria'):
            print(element)
            query_string = ('INSERT INTO accounts_appraisalformat (format_name, criteria_name)'
                            'VALUES(%s, %s)')
            data_criteria = (request.POST['format_name'], element)
            cursor.execute(query_string, data_criteria)
            db.commit()
        return HttpResponseRedirect("/appraisal_format/")

    return render(request, 'registration/appraisal_format.html', {'username': username,
                                                                  'criteria_list': cursor.fetchall(),
                                                                  'format_list': format_list.fetchall(),
                                                                  'format_set': format_set,
                                                                  'allow': check_permission(username, 3)})


@csrf_exempt
@login_required
def roles_functions(request):
    username = None
    users = None
    roles = None
    functions = None
    roles_functions = None
    role_id = 1
    different_functions = list()
    different_roles = list()

    if request.user.is_authenticated:
        username = request.user.get_username()

    if request.method == 'GET':
        db = MySQLdb.connect(user='root', db='mydb', passwd='123', host='localhost')

        users = db.cursor(MySQLdb.cursors.DictCursor)
        users.execute('SELECT * FROM auth_user ORDER BY id')

        roles = db.cursor(MySQLdb.cursors.DictCursor)
        roles.execute('SELECT * FROM accounts_roles ORDER BY id')

        functions = db.cursor(MySQLdb.cursors.DictCursor)
        functions.execute('SELECT * FROM accounts_functions ORDER BY id')

        roles_functions = db.cursor(MySQLdb.cursors.DictCursor)
        roles_functions.execute('SELECT * FROM accounts_rolesfunctions ORDER BY id')

        functions_set = set()
        for element in functions:
            functions_set.add(element['id'])

        get_roles = db.cursor(MySQLdb.cursors.DictCursor)
        get_roles.execute('SELECT * FROM accounts_roles ORDER BY id')

        for role in get_roles.fetchall():
            func_in_roles = set()
            get_rolesfunctions = db.cursor(MySQLdb.cursors.DictCursor)
            get_rolesfunctions.execute('SELECT * FROM accounts_rolesfunctions ORDER BY role_id')
            for element in get_rolesfunctions.fetchall():
                if element['role_id'] == role['id']:
                    func_in_roles.add(element['function_id'])
            different_functions = different_functions + [{'id': role['id'],
                                                          'diff_func': list(functions_set.difference(func_in_roles))}]
        print(different_functions)

        get_roles = db.cursor(MySQLdb.cursors.DictCursor)
        get_roles.execute('SELECT * FROM accounts_roles ORDER BY id')
        roles_set = set()
        for element in get_roles:
            roles_set.add(element['id'])

        user_roles = db.cursor(MySQLdb.cursors.DictCursor)
        user_roles.execute('SELECT * FROM auth_user ORDER BY role_id')
        for user in user_roles.fetchall():
            role_of_user = set()
            get_roles = db.cursor(MySQLdb.cursors.DictCursor)
            get_roles.execute('SELECT * FROM accounts_roles ORDER BY id')
            for role in get_roles.fetchall():
                if user['role_id'] == role['id']:
                    role_of_user.add(user['role_id'])
            different_roles = different_roles + [{'id': user['id'],
                                                  'diff_roles': list(roles_set.difference(role_of_user))}]
        print(different_roles)
    if request.method == 'POST':
        db = MySQLdb.connect(user='root', db='mydb', passwd='123', host='localhost')
        if "functions2roles" in request.POST:
            del_functions = db.cursor(MySQLdb.cursors.DictCursor)
            query_string = str('DELETE FROM accounts_rolesfunctions WHERE role_id = {};').format(request.POST['role'])
            del_functions.execute(query_string)
            db.commit()
            add_functions = db.cursor(MySQLdb.cursors.DictCursor)
            rolesfunctions = db.cursor(MySQLdb.cursors.DictCursor)
            rolesfunctions.execute('SELECT MAX(ID) FROM accounts_rolesfunctions')
            max_id = None
            for element in list(rolesfunctions):
                max_id = int(element['MAX(ID)'])

            for element in request.POST.getlist('list_functions'):
                print(element)
                query_str = ('INSERT INTO accounts_rolesfunctions (id, role_id, function_id)'
                             'VALUES(%s, %s, %s)')
                data = (max_id + 1, request.POST['role'], element)
                add_functions.execute(query_str, data)
                db.commit()
                max_id = max_id + 1

        elif "roles2users" in request.POST:
            if len(request.POST.getlist('role')) == 1:
                query_str = ('UPDATE auth_user SET role_id = {} '
                             'WHERE id = {}').format(int(request.POST['role']), request.POST['user'])
                update_role = db.cursor(MySQLdb.cursors.DictCursor)
                update_role.execute(query_str)
                db.commit()

        return HttpResponseRedirect("/roles_functions/")

    return render(request, 'registration/roles_functions.html', {'users': users.fetchall(),
                                                                 'username': username, 'roles': roles.fetchall(),
                                                                 'functions': functions, 'role_id': role_id,
                                                                 'rolesfunctions': roles_functions.fetchall(),
                                                                 'different_functions': different_functions,
                                                                 'different_roles': different_roles,
                                                                 'allow': check_permission(username, 2)})
